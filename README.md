# RIFT
## A Remnux and SIFT Vagrant instance for DFIR/Malware analysts.

### What is it?
<p>RIFT is essentially just a Vagrantfile and an initialization script. The Vagrantfile comes equiped with options that easily allow you to customize a Virtualbox-based Vagrant instance. The initialization script is what configures and installs everything on the Vagrant instance. The ubuntu/bionic64 image was used as the base image for the construction of these files and is recommended.</p>

### What's included?
SIFT<br>
REmnux<br>

### First initialization
<p>First initialization will take some time. Please review the initialization script in case you are experiencing any issues or are just curious.</p>

`git clone https://gitlab.com/mynameisgeff/RIFT.git`<br>
`cd RIFT`<br>
`export VAGRANT_EXPERIMENTAL="disks"`<br>
`vagrant box add ubuntu/bionic64`<br>
`vagrant up --provider=virtualbox` or `vagrant up`
