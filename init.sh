!/bin/sh
sudo apt update
sudo apt full-upgrade -y
sudo apt install ubuntu-desktop -y
wget https://REMnux.org/remnux-cli
mv remnux-cli remnux
chmod +x remnux
sudo mv remnux /usr/local/bin
sudo remnux install --mode=addon --user=vagrant
sudo curl -Lo /usr/local/bin/sift https://github.com/sans-dfir/sift-cli/releases/download/v1.9.2/sift-cli-linux
chmod +x /usr/local/bin/sift
sift install
echo "All set! Happy forensicating!"
echo "<3 weff"
sudo reboot